import { execSync } from 'child_process';

console.log("Hello, I am a typescript app within a snap running on a %s machine!", process.arch);

const snapDir = process.env.SNAP;
console.log("- current directory = %s, $SNAP = %s!", __dirname, snapDir);
console.log("- running node version = %s in path %s!", process.version, process.execPath);
console.log("- environment: ", process.env);
console.log("");

console.log("Testing ZIP library dependency by packaging some *.sh files:");
try {
    const result = execSync("zip -8 -v $SNAP_USER_COMMON/hello.zip $(ls -1 $SNAP/*.sh | sort)");
    console.log("-> successful zipped: %s", result);    
 } catch (error) {
    console.error(`Failed to ZIP files: ${error}`);
}

console.log("");

const curlTestUrl = "https://postman-echo.com/ip";
console.log("Testing cURL library dependency by downloading json file from '%s':", curlTestUrl);
try {
    const result = execSync(`curl ${curlTestUrl}`);
    console.log("-> successful downloaded: %s", result);
} catch (error) {
    console.error(`Failed to download file via curl: ${error}`);
}

console.log("");

console.log("Mount samba share");
try {
    const result = execSync(`mount.cifs -v -o credentials=$SNAP_COMMON/.pass //192.168.1.110/sambaShare $SNAP_DATA/sambaClientNodeJs`);
    console.log("-> Mount samba share: %s", result);
} catch (error) {
    console.error(`Failed to mount: ${error}`);
}

console.log("");

console.log("Print mounted share");
try {
    const result = execSync(`ls -al $SNAP_DATA/sambaClientNodeJs`);
    console.log("-> Print mounted share: %s", result);
} catch (error) {
    console.error(`Failed to read folder: ${error}`);
}

console.log("");

console.log("Unmount samba share");
try {
    const result = execSync(`umount -v $SNAP_DATA/sambaClientNodeJs`);
    console.log("-> Unmount samba share: %s", result);
} catch (error) {
    console.error(`Failed to unmount: ${error}`);
}

console.log("");